import requests, json, m3u8, os, urllib, binascii, configparser
Config = configparser.ConfigParser()
Config.read("config.ini")
config = Config["DEFAULT"]

def ppjson(json_thing, sort=True, indents=4):
    if type(json_thing) is str:
        print(json.dumps(json.loads(json_thing), sort_keys=sort, indent=indents))
    else:
        print(json.dumps(json_thing, sort_keys=sort, indent=indents))
    return None

token = config["token"]

def makeRequest(method, path, data=None, auth=True, streamRequest=True, jsonResponse=True, base_url=config["base_url"]):
    headers = { "appVersion": config["app_version"] }
    if auth:
        headers["Authorization"] = "Bearer " + token
    if method == "GET":
        r = requests.get(base_url + path, headers=headers, stream=streamRequest)
    elif method == "POST":
        if data:
            r = requests.post(base_url + path, headers=headers, data=data)
        else:
            r = requests.post(base_url + path, headers=headers)
    if jsonResponse:
        # print(r.text)
        return r.json()
    else:
        return r.text

def login(loginCreds=None):
    global token
    if not loginCreds:
        loginCreds = {"username": config["username"], "password": config["password"], "deviceId": config["deviceId"], "deviceName": config["deviceName"], "platform": config["platform"]}
    token = makeRequest("POST", "/api/auth/signin", loginCreds)["token"]
    return token
# login(loginCreds)

def getProfile():
    return makeRequest("GET", "/api/user/profile")
# getProfile()

def getRecentlyWatched():
    return makeRequest("GET", "/api/user/recentlyWatched")
#getRecentlyWatched()

def getSubjects(cached_only=False):
    if cached_only:
        with open("ref/subjects.json", "r") as f:
            return json.load(f)
    return makeRequest("GET", "/api/subjects")
#getSubjects()

def getSubjectInfo(subjectId, sessionId):
    return makeRequest("GET", "/api/subjects/"+subjectId+"/info/"+sessionId)
# getSubjectInfo("105443", "526")

def getLectures(subjectId, sessionId, cached_only=False):
    if cached_only:
        with open("ref/lectures.json", "r") as f:
            lectures = json.load(f)
    return makeRequest("GET", "/api/subjects/"+subjectId+"/lectures/"+sessionId)
#getLectures("105443", "526")

def getVideoInfo(videoId):
    return makeRequest("GET", "/api/videos/params/id/"+videoId)
#getVideoInfo("966383")

def getCatalog(cached_only=False):
    if cached_only:
        with open("ref/catalog.json", "r") as f:
            return json.load(f)
    searchQuery = {"pageSize": 10, "pageNumer": "1", "searchParams": {}}
    return makeRequest("POST", "/api/subjects/catalog2", searchQuery)
# getCatalog()

def getAvailableStreams(ttid):
    m_playlists = m3u8.load(config["base_external_ip"]+"/api/fetchvideo?type=index.m3u8&ttid="+ttid+"&token="+token)
    playlists = []
    for playlist in m_playlists.playlists:
        p = m3u8.load(playlist.uri)
        if p.segments:
            playlists.append((playlist.stream_info.bandwidth,p)) # (bandwidth, object)
    return playlists
    # return m3u8.loads(makeRequest("GET", "/api/fetchvideo?type=index.m3u8&ttid="+ttid+"&token="+token, auth=False, jsonResponse=False, base_url=config["base_external_ip"]))
# getAvailableStreams("1889384")

def downloadStream(stream, location):
    encLoc = os.path.join(location, "encrypted")
    decLoc = os.path.join(location, "decrypted")
    keysLoc = os.path.join(location, "keys")
    for f in [location, encLoc, decLoc, keysLoc]:
        os.makedirs(f, exist_ok=True)
    
    # Download all keys
    for key in stream.keys:
        keyUrl = key.uri
        keyName = "key_" + keyUrl[keyUrl.rfind('=')+1:]
        print("Downloading key", keyName)
        r = requests.get(keyUrl, headers={"appVersion": config["app_version"], "Authorization": "Bearer "+token})
        with open(os.path.join(keysLoc, keyName), "wb") as f:
            for chunk in r.iter_content(1024):
                if chunk:
                    f.write(chunk)
    
    # Download encrypted segments (and call decryptSegment)
    for segment in stream.segments:
        segUrl = urllib.parse.unquote(segment.uri)
        segName = segUrl[segUrl.rfind('/')+1:]
        print("Downloading segment", segName)
        r = requests.get(segUrl, stream=True, headers={"appVersion": config["app_version"], "Authorization": "Bearer "+token})
        with open(os.path.join(encLoc, segName), "wb") as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:
                    f.write(chunk)
        decryptSegment(os.path.join(encLoc, segName), keysLoc, decLoc)
    
    # Merge all segments
    mergeSegments(decLoc, location)

def decryptSegment(segLoc, keyDir, decLoc):
    keyNo = segLoc[segLoc.rfind('_')+1:-3]
    keyLoc = os.path.join(keyDir, "key_"+keyNo)
    print("Decrypting segment", os.path.split(segLoc)[1])
    with open(keyLoc, "rb") as keyfile:
        key = binascii.hexlify(keyfile.read())
    iv = segLoc.split("_")[2].zfill(32)
    cmd = "openssl aes-128-cbc -d -in " + segLoc + " -out " + os.path.join(decLoc, os.path.split(segLoc)[1]) + " -K " + key.decode() + " -iv " + iv
    os.system(cmd)

def mergeSegments(decLoc, outLoc):
    outLoc = os.path.join(outLoc, "out.ts")
    print("Merging all files in", decLoc, "to", outLoc)
    filelist = os.listdir(decLoc)
    filelist.sort()
    with open(outLoc, "wb") as outfile:
        for filename in filelist:
            if filename.endswith(".ts"):
                print("Merging", filename)
                with open(os.path.join(decLoc, filename), "rb") as f:
                    while True:
                        data = f.read(65536)
                        if data:
                            outfile.write(data)
                        else:
                            break
