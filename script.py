from prettytable import PrettyTable
import json, os
import sdk

sessionId = "526"

subjects = sdk.getSubjects(cached_only=True)

t = PrettyTable(['subjectId', 'subjectName', 'sessionId', 'sessionName', 'professorName'])
for sub in subjects:
    t.add_row([ sub["subjectId"], sub["subjectName"], sub["sessionId"], sub["sessionName"], sub["professorName"] ])
print(t)
subjectId = input("Enter subjectId: ")

lectures = sdk.getLectures(subjectId, sessionId)
lectures.sort(key=lambda x: x["seqNo"], reverse=False)

t = PrettyTable(['seqNo', 'topic', 'actualDuration', 'classroomId', 'status', 'ttid', 'videoId'])
for lec in lectures:
    t.add_row([ lec["seqNo"], lec["topic"], lec["actualDuration"], lec["classroomId"], lec["status"], lec["ttid"], lec["videoId"] ])
print(t)
ttid = input("Enter ttid: ")

# Check if ttid folder exists
if os.path.exists("data/"+ttid):
    if input("This folder already exists. Continue?") not in ["yes", "YES", "yes", "y", "Y"]:
        print("Exiting.")
        exit()

# subjectId = "105443"
# sessionId = "526"
# ttid = "1889384"

c = 1
streams = sdk.getAvailableStreams(ttid)

for stream in streams:
    print(c, " - ", stream[0])
    c += 1
streamId = int(input("Pick a stream quality: "))

sdk.downloadStream(streams[streamId-1][1], "data/"+ttid)
print("Done!")
