import binascii, os

with open("1889384/key_00", "rb") as keyfile:
    key = binascii.hexlify(keyfile.read())
    print(key)
# with open("1889384/key_00", "rb") as keyfile:
#     for chunk in iter(lambda: keyfile.read(32), b''):
#         key = binascii.hexlify(chunk)
#         print(key)
iv = "00000000000000000000000000000000"

vidname = "1889384/800x450_27v1_0000_hls_0.ts"
os.system("openssl aes-128-cbc -d -in " + vidname + " -out decrypted/" + vidname + " -K " + key + " -iv " + iv)
